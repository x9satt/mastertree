# -*- coding:utf-8 -*-
from django.shortcuts import render, redirect, get_object_or_404, render_to_response
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic
from django.template import RequestContext
from .models import Item, Category, ItemImage, SliderImage, ItemComment, Article
from django.core.mail import send_mail, EmailMultiAlternatives

from django.core import urlresolvers
from cart import cart
from .forms import ItemAddToCartForm


#def index(request, template_name='shop/index.html'):
#    page_title = 'Резные иконы'
#    return render_to_response(template_name, locals(), context_instance=RequestContext(request))

def subscribe(request):
    if request.method == 'POST':
        postdata = request.POST.copy()
        submail = postdata.get('submail','')
        sub_subject, sub_from_email, sub_to = 'Добавление подписчика', 'sender@mastertree.ru', 'info@mastertree.ru'
        sub_text_content = ''
        sub_html_content = '<p> Почта: '+ submail +';</p>'
        sub_msg = EmailMultiAlternatives(sub_subject, sub_text_content, sub_from_email, [sub_to])
        sub_msg.attach_alternative(sub_html_content, "text/html")
        sub_msg.send()
        return redirect(request.META['HTTP_REFERER'])
    else: return HttpResponseRedirect(reverse('home'))

def callback(request):
    if request.method == 'POST':
        postdata = request.POST.copy()
        callphone = postdata.get('callphone','')
        call_subject, call_from_email, call_to_email = 'Обратный звонок', 'sender@mastertree.ru', 'info@mastertree.ru'
        call_text_content = ''
        call_html_content = '<p> Телефон: '+ callphone +';</p>'
        call_msg = EmailMultiAlternatives(call_subject, call_text_content, call_from_email, [call_to_email])
        call_msg.attach_alternative(call_html_content, "text/html")
        call_msg.send()
        return redirect(request.META['HTTP_REFERER'])
    else: return HttpResponseRedirect(reverse('home'))

def add_comment(request):
    if request.method == 'POST':
        postdata = request.POST.copy()
		############################################
        i = postdata.get('item','')
        item = Item.objects.get(id=i)
        item_name = item.item_name
        comment = postdata.get('comment','')
        comm_name = postdata.get('comm_name','')
        comm_email = postdata.get('comm_email','')
        ic = ItemComment()
        ic.item = item
        ic.item_name = item_name
        ic.comment = comment
        ic.comm_name = comm_name
        ic.comm_email = comm_email
        ic.save()
        return redirect(request.META['HTTP_REFERER'])
    else: return HttpResponseRedirect(reverse('home'))

def show_category(request, category_slug, template_name='shop/category.html'):
    c = get_object_or_404(Category, slug=category_slug)
    items = c.item_set.all()
    page_title = c.category_name
    meta_keywords = c.meta_keywords
    meta_description = c.meta_description
    return render_to_response(template_name, locals(), context_instance=RequestContext(request))


def show_item(request, item_slug, template_name='shop/item.html'):
    i = get_object_or_404(Item, slug=item_slug)
    categories = i.categories.filter(is_active=True)
    page_title = i.item_name
    meta_keywords = i.meta_keywords
    meta_description = i.meta_description
    comments = i.itemcomment_set.all()

    if request.method == 'POST':
        postdata = request.POST.copy()
        form = ItemAddToCartForm(request, postdata)
        if form.is_valid():
            cart.add_to_cart(request)
            if request.session.test_cookie_worked():
                request.session.delete_test_cookie()
            url = urlresolvers.reverse('cart:show_cart')
            #return HttpResponseRedirect(url) - перенаправление в корзину(убрано)
            return redirect(request.META['HTTP_REFERER'])
            #return render_to_response(template_name, locals(), context_instance=RequestContext(request)) - так оно думало, что кук нет
    else:
        form = ItemAddToCartForm(request=request, label_suffix=':')
    form.fields['item_slug'].widget.attrs['value'] = item_slug
    request.session.set_test_cookie()
#    return render_to_response("shop/item.html", locals(), context_instance=RequestContext(request))

    return render_to_response(template_name, locals(), context_instance=RequestContext(request))



def show_article(request, article_slug, template_name='shop/article.html'):
    a = get_object_or_404(Article, slug=article_slug)
    page_title = a.article_name
    meta_keywords = a.meta_keywords
    meta_description = a.meta_description

    return render_to_response(template_name, locals(), context_instance=RequestContext(request))

def NewsView(request, template_name='shop/news.html'):
    page_title = 'Новости'
    meta_keywords = 'Новости, Новости Master Tree'
    meta_description = 'Новости MasterTree'
    return render_to_response(template_name, locals(), context_instance=RequestContext(request))



class IndexView(generic.ListView):
    template_name = 'shop/index.html'
    context_object_name = 'slider_images'

    def get_queryset(self):
        return SliderImage.objects.all()


class TestIndexView(generic.ListView):
    template_name = 'shop/testindex.html'
    context_object_name = 'slider_images'
    def get_queryset(self):
        return SliderImage.objects.all()

#class ShopView(generic.ListView):
#    template_name = 'shop/shop.html'
#
#    def get_queryset(self):
#        return Item.objects.all()

def ShopView(request, template_name='shop/shop.html'):
    page_title = 'Каталог'
    meta_keywords = 'Каталог, Резные иконы'
    meta_description = 'Каталог MasterTree'
    return render_to_response(template_name, locals(), context_instance=RequestContext(request))

def AboutView(request, template_name="shop/about.html"):
    page_title = 'О нас'
    meta_keywords = 'О нас, MasterTree, Адрес MasterTree, Телефоны MasterTree'
    meta_description = 'О MasterTree'
    return render_to_response(template_name, locals(), context_instance=RequestContext(request))

def PayView(request, template_name="shop/pay.html"):
    page_title = 'Доставка и оплата'
    meta_keywords = 'Доставка и оплата, MasterTree, Доставка и оплата MasterTree'
    meta_description = 'Доставка и оплата MasterTree'
    return render_to_response(template_name, locals(), context_instance=RequestContext(request))

def ContactsView(request, template_name="shop/contacts.html"):
    if request.method == 'POST':
        postdata = request.POST.copy()
        mess_name = postdata.get('mess_name','')
        mess_email = postdata.get('mess_email','')
        mess_subject = postdata.get('mess_subject','')
        mess_message = postdata.get('mess_message','')
        mess_from_email, mess_to = 'sender@mastertree.ru', 'info@mastertree.ru'
        mess_text_content = ''
        mess_html_content = '<p> ФИО: ' + mess_name +';</p><p> Почта: '+ mess_email +';</p><br><p> Сообщение: </p><p>' + mess_message + '</p>'
        mess_msg = EmailMultiAlternatives(mess_subject, mess_text_content, mess_from_email, [mess_to])
        mess_msg.attach_alternative(mess_html_content, "text/html")
        mess_msg.send()
        return redirect(request.META['HTTP_REFERER'])
    page_title = 'Контакты'
    meta_keywords = 'Контакты, MasterTree, Адрес MasterTree, Телефоны MasterTree'
    meta_description = 'Контакты MasterTree'
    return render_to_response(template_name, locals(), context_instance=RequestContext(request))


def Contacts2View(request, template_name="shop/contacts2.html"):
    if request.method == 'POST':
        postdata = request.POST.copy()
        mess_name = postdata.get('mess_name','')
        mess_email = postdata.get('mess_email','')
        mess_subject = postdata.get('mess_subject','')
        mess_message = postdata.get('mess_message','')
        mess_from_email, mess_to = 'sender@mastertree.ru', 'info@mastertree.ru'
        mess_text_content = ''
        mess_html_content = '<p> ФИО: ' + mess_name +';</p><p> Почта: '+ mess_email +';</p><br><p> Сообщение: </p><p>' + mess_message + '</p>'
        mess_msg = EmailMultiAlternatives(mess_subject, mess_text_content, mess_from_email, [mess_to])
        mess_msg.attach_alternative(mess_html_content, "text/html")
        mess_msg.send()
        return redirect(request.META['HTTP_REFERER'])
    page_title = 'Контакты'
    meta_keywords = 'Контакты, MasterTree, Адрес MasterTree, Телефоны MasterTree'
    meta_description = 'Контакты MasterTree'
    return render_to_response(template_name, locals(), context_instance=RequestContext(request))


#def detail(request, pk):
 #   item = get_object_or_404(Item, pk=pk)
  #  return render(request, 'shop/detail.html', {'item': item})

#class DetailView(generic.DetailView):
#    model = Item
#    template_name = 'shop/detail.html'
#    def item_page(request, pk):
#        item = Item.objects.get(pk=pk)
#        image_list = item.images.all()

#    def get_queryset(self):
#        return Item.objects.all()
#        image_list = item.images.all()