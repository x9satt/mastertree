from django.contrib import admin
from .models import Item, Category, ItemImage, SliderImage, ItemComment, Article, ArticleImage
from .forms import ItemAdminForm
from django.utils.html import format_html
from sorl.thumbnail import get_thumbnail



class ImagesInLine(admin.TabularInline):
    model = ItemImage
    extra = 3

class ArticleImagesInLine(admin.TabularInline):
    model = ArticleImage
    extra = 3

class ItemAdmin(admin.ModelAdmin):
#   fieldsets = [(None,{'fields':['item_name', 'categories']}),('Превью',{'fields':['preview']}),('Наличие и цена', {'fields':['quantity', 'price']}),('Описание', {'fields':['description']})]
    inlines = [ImagesInLine]
    list_display = ( 'item_name', 'quantity', 'price', 'created_at', 'updated_at',)
    list_display_links = ('item_name',)
    list_per_page = 50
    ordering = ['-created_at']
    list_filter = ['item_name']
    search_fields = ['item_name', 'description', 'meta_keywords', 'meta_description']
    exclude = ('created_at', 'updated_at', 'is_featured')
    
    #sets up slug to be generated from item name
    prepopulated_fields = {'slug':('item_name',)}

    def thumb(self, obj):
        im = get_thumbnail(obj.preview, '60x60', quality=99)
        return format_html('<img src="{}" border="0" alt="" width="{}" height="{}" />', im.url, im.width, im.height)

    thumb.short_description = 'Фото'
    thumb.allow_tags = True



class CategoryAdmin(admin.ModelAdmin):
    list_display = ('number', 'category_name', 'created_at', 'updated_at',)
    list_display_links = ('category_name',)
    list_per_page = 20
    ordering = ['number']
    search_fields = ['category_name', 'description', 'meta_keywords', 'meta_description']
    exclude = ('created_at', 'updated_at',)

    prepopulated_fields = {'slug':('category_name',)}



class SliderAdmin(admin.ModelAdmin):
    list_display = ('thumb', 'description', 'alt', 'created_at', 'updated_at',)
    ordering = ['-created_at']
    search_fields = ['description', 'alt']
    exclude = ('created_at', 'updated_at')

    def thumb(self, obj):
        im = get_thumbnail(obj.img, '100x100', quality=99)
        return format_html('<img src="{}" border="0" alt="" width="{}" height="{}" />', im.url, im.width, im.height)
    
    thumb.shor_description = 'Фото для слайдера'
    thumb.allow_tags = True



class ItemCommentAdmin(admin.ModelAdmin):
    list_display = ('item_name', 'created_at', 'comment', 'comm_name', 'comm_email',)
    ordering = ['-created_at']
    search_fields = ['item']


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('article_name', 'created_at', 'updated_at',)
    ordering = ['-updated_at', '-created_at']
    search_fields = ['article_name']
    inlines = [ArticleImagesInLine]

    prepopulated_fields = {'slug':('article_name',)}


admin.site.register(Item, ItemAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(SliderImage, SliderAdmin)
admin.site.register(ItemComment, ItemCommentAdmin)
admin.site.register(Article, ArticleAdmin)
# Register your models here.