# -*- coding: utf-8 -*-

from django import forms
from .models import Item



class ItemAdminForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = '__all__'
        def clean_price(self):
            if self.cleaned_data['price'] <= 0:
                raise forms.ValidationError('Price must be greater than zero.')
            return self.cleaned_data['price']

class ItemAddToCartForm(forms.Form):
    #quantity = forms.IntegerField(widget=forms.TextInput(attrs={'size':'2', 'value':'1', 'class':'quantity'}), error_messages={'invalid':'Введите правильное число.'}, min_value=1)
    item_slug = forms.CharField(widget=forms.HiddenInput())

    def __init__(self, request=None, *args, **kwargs):
        self.request = request
        super(ItemAddToCartForm, self).__init__(*args, **kwargs)

    def clean(self):
        if self.request:

            if not self.request.session.test_cookie_worked():
                raise forms.ValidationError("Куки должны быть включены.")
        return self.cleaned_data