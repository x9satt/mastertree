# -*- coding:utf-8 -*-
from django.db import models
from django.core.urlresolvers import reverse
from django.core.files.storage import FileSystemStorage
from sorl.thumbnail import get_thumbnail 
import uuid
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver

fs = FileSystemStorage(location='shop/static/shop/images/db_items_images')


def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return 'static/img/%s/%s/%s' % (filename[:1], filename[2:3], filename)

class SliderImage(models.Model):
    img = models.ImageField(upload_to=get_file_path)
    link = models.TextField(blank=True)
    description = models.TextField()
    alt = models.TextField(help_text='Текст-описание картинки, для тех у кого нет картинок и поисковых роботов')
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    class Meta:
        db_table = 'slider_images'
        ordering = ['-created_at']
        

    def __unicode__(self):
        return self.alt

@receiver(pre_delete, sender=SliderImage)
def SliderImage_delete(sender, instance, **kwargs):
    # Pass false so FileField doesn't save the model.
    instance.img.delete(False)

class Category(models.Model):
    preview = models.ImageField(blank=True, upload_to=get_file_path)
    category_name = models.CharField(max_length=255, default='icon')
    slug = models.SlugField(max_length=255, unique=True,
        help_text='Unique value for product page URL, created from name.')
    description = models.TextField()
    is_active = models.BooleanField(default=True)
    meta_keywords = models.TextField("Meta Keywords", help_text = 'Comma-delimited set of SEO keywords for meta tag')
    meta_description = models.TextField("Meta Description", help_text = 'Content for description meta tag')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    number = models.IntegerField(default=0, unique=True)

    class Meta:
        db_table = 'categories'
        ordering = ['number']
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.category_name

    @models.permalink
    def get_absolute_url(self):
        return ('shop:shop_category', (), { 'category_slug': self.slug })

@receiver(pre_delete, sender=Category)
def Category_delete(sender, instance, **kwargs):
    # Pass false so FileField doesn't save the model.
    instance.preview.delete(False)

class Item(models.Model):
    item_name = models.CharField(max_length=255, unique=True)
#    item_image = models.ImageField(blank=True, upload_to='static/shop/images/')
    slug = models.SlugField(max_length=255, unique=True,
        help_text = 'Unique value for product page URL, created from name.')
    #sku = models.CharField(max_length=50)
    price = models.DecimalField(max_digits=9, decimal_places=2)
    old_price = models.DecimalField(max_digits=9, decimal_places=2, blank=True, default=0.00)
    #image = models.CharField(max_length=50)
    
    is_active = models.BooleanField(default=True)
    is_bestseller = models.BooleanField(default=False)
    is_featured = models.BooleanField(default=False)

    quantity = models.IntegerField(default=0)
    categories = models.ManyToManyField(Category)
    #price = models.CharField(max_length=50, default='Цена по запросу')
    description = models.TextField()
    about = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    #meta_keywords = models.CharField(max_length=255, help_text='Comma-delimited set of SEO keywords for meta tag')
    meta_keywords = models.TextField(help_text='Comma-delimited set of SEO keywords for meta tag')
    #meta_description = models.CharField(max_length=255, help_text='Content for description meta tag')
    meta_description = models.TextField(help_text='Content for description meta tag')

    preview = models.ImageField(blank=False, default='static/shop/images/previews/default.png', upload_to=get_file_path)
    
    class Meta:
        db_table = 'items'
        ordering = ['-is_bestseller', '-price', '-updated_at']


    def __unicode__(self):
        return self.item_name



    @models.permalink
    def get_absolute_url(self):
        return ('shop:shop_item', (), { 'item_slug': self.slug })

    def sale_price(self):
        if self.old_price > self.price:
            return self.price
        else:
            return None

#    def get_image_100x100(self):
#        return get_thumbnail(self.item_image, '100x100', crop='center')

@receiver(pre_delete, sender=Item)
def Item_delete(sender, instance, **kwargs):
    # Pass false so FileField doesn't save the model.
    instance.preview.delete(False)

class ItemImage(models.Model):
    item = models.ForeignKey(Item, related_name='images', on_delete=models.CASCADE)
    item_image = models.ImageField(blank=True, upload_to=get_file_path)
    def get_image_100x100(self):
        return get_thumbnail(self.item_image, '100x100', crop='center')

@receiver(pre_delete, sender=ItemImage)
def ItemImage_delete(sender, instance, **kwargs):
    # Pass false so FileField doesn't save the model.
    instance.item_image.delete(False)


class ItemComment(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    item_name = models.CharField(max_length=255, null=True)
    comment = models.TextField()
    comm_name = models.CharField(max_length=255, blank=True)
    comm_email = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'comments'
        ordering = ['-created_at']

    def __unicode__(self):
        return self.comment

class Article(models.Model):
    article_name = models.CharField(max_length=191, unique=True, help_text = 'Не больше 191 символа, аккуратнее')
    slug = models.SlugField(max_length=191, unique=True, help_text = 'Unique value for article page URL, created from name.')
    preview = models.ImageField(blank=False, upload_to=get_file_path)
    #article_short_text = models.TextField(max_length=255, help_text = 'Отображается в списке новостей и на главной')
    article_text = models.TextField()
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    meta_keywords = models.TextField(help_text='Comma-delimited set of SEO keywords for meta tag')
    meta_description = models.TextField(help_text='Content for description meta tag')


    class Meta:
        db_table = 'articles'
        ordering = ['-created_at']

    def __unicode__(self):
        return self.article_name

    def get_image_100x100(self):
        return get_thumbnail(self.article_image, '100x100', crop='center')


    @models.permalink
    def get_absolute_url(self):
        return ('shop_article', (), { 'article_slug': self.slug })

@receiver(pre_delete, sender=Article)
def Article_delete(sender, instance, **kwargs):
    # Pass false so FileField doesn't save the model.
    instance.preview.delete(False)


class ArticleImage(models.Model):
    article = models.ForeignKey(Article, related_name='images', on_delete=models.CASCADE)
    article_image = models.ImageField(blank=True, upload_to=get_file_path)

@receiver(pre_delete, sender=ArticleImage)
def ArticleImage_delete(sender, instance, **kwargs):
    # Pass false so FileField doesn't save the model.
    instance.article_image.delete(False)



class Client(models.Model):
    client_name = models.CharField(max_length=100)
    phone = models.CharField(max_length=50, blank=True)
    adress = models.CharField(max_length=200, blank=True)
    email = models.EmailField(max_length=100, blank=True)


class Sale_Status(models.Model):
    FORMING = 'declared here'
    CONFIRMATION = 'declared here'
    WAITING_FOR_DELIEVERY = 'declared here'
    SOLD_WAITING_FOR_DELIEVERY = 'declared here'
    SOLD = 'declared here'

    status_choices = (
        (FORMING, 'Оформляется'),
        (CONFIRMATION, 'Ждёт подтверждения'),
        (WAITING_FOR_DELIEVERY, 'Ждёт доставки'),
        (SOLD_WAITING_FOR_DELIEVERY, 'Продано, ждёт доставки'),
        (SOLD, 'Продано'),
    )

    status_name = models.CharField(max_length=50, choices=status_choices, default=FORMING)

    def __str__(self):
        return self.status_name


class Sale(models.Model):
    WHEN_DELIVERED = 'declared here'
    CARD = 'declared here'


    payment_choices = (
        (WHEN_DELIVERED, 'Наличными курьеру'),
        (CARD, 'Оплата картой'),
    )

    client = models.ForeignKey(Client)
    item = models.ManyToManyField(Item)
    phone = models.CharField(max_length=50)
    address = models.CharField(max_length=200)
    date_created = models.DateField()
    date_modified = models.DateField()
    status = models.ForeignKey(Sale_Status)
    sale_sum = models.DecimalField(max_digits=15,decimal_places=2)
    payment = models.CharField(max_length=50, choices=payment_choices, default=WHEN_DELIVERED)


class Sale_History(models.Model):
    sale = models.ForeignKey(Sale)
    status = models.ForeignKey(Sale_Status)
    sale_sum = models.DecimalField(max_digits=15, decimal_places=2)
    active_from = models.DateTimeField()
    active_to = models.DateTimeField(blank=True)

# Create your models here.