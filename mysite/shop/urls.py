# -*- coding:utf-8 -*-
from django.conf.urls import *

from django.conf.urls import url, patterns
from . import views
#from .views import detail
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name = 'shop'
urlpatterns = [
    #url(r'^$', views.ShopView.as_view(), name='shopv'),
    url(r'^$', views.ShopView, {'template_name':'shop/shop.html'}, name='shopv'),
#    url(r'^about/$', views.AboutView, name='about'),
    url(r'^contacts/$', views.ContactsView, name='contacts'),
    #url(r'^$', 'index', {'template_name':'shop/index.html'}, 'shop_home'),
    #url(r'^category/(?P<category_slug>[-\w]+)/$', 'show_category', {'template_name':'shop/category.html'}, 'shop_category'),
    url(r'^category/(?P<category_slug>[-\w]+)/$', views.show_category, {'template_name':'shop/category.html'}, name='shop_category'),
    url(r'^item/(?P<item_slug>[-\w]+)/$', views.show_item, {'template_name':'shop/item.html'}, name='shop_item'),
    url(r'^send/$', views.subscribe, name='subscribe'),
    url(r'^call/$', views.callback, name='callback'),
    url(r'^comment/$', views.add_comment, name='add_comment'),

#    url(r'^$', views.IndexView.as_view(), name='index'),
#    url(r'^items/(?P<pk>[0-9]+)/$', detail, name='detail'),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#urlpatterns += staticfiles_urlpatterns()