# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-09-25 20:28
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0020_auto_20160914_1945'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='item',
            name='item_image',
        ),
    ]
