# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-10-13 08:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0030_auto_20161012_1159'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='number',
            field=models.IntegerField(default=0),
        ),
    ]
