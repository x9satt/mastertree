# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-20 07:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0004_auto_20160720_0708'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='item_image',
            field=models.ImageField(blank=True, upload_to='shop'),
        ),
    ]
