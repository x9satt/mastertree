# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-10-24 09:50
from __future__ import unicode_literals

from django.db import migrations, models
import shop.models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0042_auto_20161017_1205'),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('article_name', models.CharField(max_length=145, unique=True)),
                ('slug', models.SlugField(help_text='Unique value for product page URL, created from name.', max_length=145, unique=True)),
                ('article_image', models.ImageField(blank=True, upload_to=shop.models.get_file_path)),
                ('article_text', models.TextField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('meta_keywords', models.TextField(help_text='Comma-delimited set of SEO keywords for meta tag')),
                ('meta_description', models.TextField(help_text='Content for description meta tag')),
            ],
            options={
                'db_table': 'articles',
                'ordering': ['-created_at'],
            },
        ),
    ]
