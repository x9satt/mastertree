# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-09-08 22:15
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0015_auto_20160908_2215'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='category',
            table=None,
        ),
        migrations.AlterModelTable(
            name='item',
            table=None,
        ),
    ]
