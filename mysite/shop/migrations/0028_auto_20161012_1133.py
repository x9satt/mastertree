# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-10-12 11:33
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0027_itemcomment'),
    ]

    operations = [
        migrations.RenameField(
            model_name='itemcomment',
            old_name='time',
            new_name='created_at',
        ),
    ]
