# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-26 06:31
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0011_auto_20160725_1109'),
    ]

    operations = [
        migrations.CreateModel(
            name='ItemImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('item_image', models.ImageField(blank=True, upload_to='static/shop/images/')),
            ],
        ),
        migrations.RemoveField(
            model_name='item',
            name='item_image',
        ),
        migrations.AddField(
            model_name='item',
            name='price',
            field=models.CharField(default='Цена по запросу', max_length=50),
        ),
        migrations.AddField(
            model_name='itemimage',
            name='item',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='shop.Item'),
        ),
    ]
