# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-09-14 19:45
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0019_auto_20160914_1943'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='preview',
            field=models.ImageField(default='static/shop/images/previews/default.png', upload_to='static/shop/images/previews/'),
        ),
    ]
