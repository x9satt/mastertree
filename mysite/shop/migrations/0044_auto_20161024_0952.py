# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-10-24 09:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0043_article'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='article_name',
            field=models.CharField(max_length=191, unique=True),
        ),
        migrations.AlterField(
            model_name='article',
            name='slug',
            field=models.SlugField(help_text='Unique value for product page URL, created from name.', max_length=191, unique=True),
        ),
    ]
