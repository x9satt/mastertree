from django import template
from cart import cart
from shop.models import Category,Item,Article
from django.contrib.flatpages.models import FlatPage

register = template.Library()

@register.inclusion_tag("tags/cart_box.html")
def cart_box(request):
    cart_item_count = cart.cart_distinct_item_count(request)
    return {'cart_item_count':cart_item_count}


@register.inclusion_tag("tags/category_list.html")
def category_list(request_path):
    active_categories = Category.objects.filter(is_active=True)
    return {
        'active_categories':active_categories,
        'request_path':request_path
    }

@register.inclusion_tag("tags/article_list.html")
def article_list(request_path):
    active_articles = Article.objects.filter(is_active=True)
    return {
        'active_articles':active_articles,
        'request_path':request_path
    }

@register.inclusion_tag("tags/last_article.html")
def last_article_item(request_path):
    last_article = Article.objects.filter(is_active=True)[0:3]
    return {
        'last_article':last_article,
        'request_path':request_path
    }

@register.inclusion_tag("tags/footer.html")
def footer_links():
    flatpage_list = FlatPage.objects.all()
    return {'flatpage_list':flatpage_list}

@register.inclusion_tag("tags/item_list.html")
def item_list(request_path):
    active_items = Item.objects.filter(is_active=True)
    return {
        'active_items':active_items,
        'request_path':request_path
    }