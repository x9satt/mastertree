"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from shop import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name="home"),
	url(r'^testindex$', views.TestIndexView.as_view(), name="testhome"),
    url(r'^contacts/$', views.ContactsView, name="contacts"),
    #url(r'^contacts2/$', views.Contacts2View, name="contacts2"),
    url(r'^about/$', views.AboutView, name="about"),
    url(r'^pay/$', views.PayView, name="pay"),
    url(r'^shop/', include('shop.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^cart/', include('cart.urls')),
    url(r'^news/$', views.NewsView, {'template_name':'shop/news.html'}, name='news'),
    url(r'^news/(?P<article_slug>[-\w]+)/$', views.show_article, {'template_name':'shop/article.html'}, name='shop_article'),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#urlpatterns += staticfiles_urlpatterns()