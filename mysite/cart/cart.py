from .models import CartItem
from shop.models import Item
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect

import decimal
import random


CART_ID_SESSION_KEY = 'cart_id'

def _cart_id(request):
    if request.session.get(CART_ID_SESSION_KEY, "") == '':
        request.session[CART_ID_SESSION_KEY] = _generate_cart_id()
    return request.session[CART_ID_SESSION_KEY]

def _generate_cart_id():
    cart_id = ''
    characters = 'ABCDEFGHIJKLMNOPQESTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!@#$%^&*()'
    cart_id_length = 50
    for y in range(cart_id_length):
        cart_id += characters[random.randint(0, len(characters)-1)]
    return cart_id

def get_cart_items(request):
    return CartItem.objects.filter(cart_id=_cart_id(request))

def add_to_cart(request):
    postdata = request.POST.copy()
    item_slug = postdata.get('item_slug','')
    quantity = postdata.get('quantity', 1)
    i = get_object_or_404(Item, slug=item_slug)
    cart_items = get_cart_items(request)
    item_in_cart = False

    for cart_item in cart_items:
        if cart_item.item.id == i.id:
            cart_item.augment_quantity(quantity)
            item_in_cart = True
    if not item_in_cart:
        ci = CartItem()
        ci.item = i
        ci.quantity = quantity
        ci.cart_id = _cart_id(request)
        ci.save()

def cart_distinct_item_count(request):
    return get_cart_items(request).count()

def get_single_item(request, item_id):
    return get_object_or_404(CartItem, id=item_id, cart_id=_cart_id(request))

# update quantity for single item
def update_cart(request):
    postdata=request.POST.copy()
    item_id = postdata['item_id']
    quantity = postdata['quantity']
    cart_item = get_single_item(request, item_id)
    if cart_item:
        if int(quantity) > 0:
            cart_item.quantity = int(quantity)
            cart_item.save()
        else:
            remove_from_cart(request)

#remove a single item from cart
def remove_from_cart(request):
    postdata = request.POST.copy()
    item_id = postdata['item_id']
    cart_item = get_single_item(request, item_id)
    if cart_item:
        cart_item.delete()

#gets the total cost for the current cart
def cart_subtotal(request):
    cart_total = decimal.Decimal('0.00')
    cart_items = get_cart_items(request)
    for cart_item in cart_items:
        cart_total += cart_item.item.price * cart_item.quantity
    return cart_total

def checkout(request):
    postdata=request.POST.copy()
    cart_items = get_cart_items(request)
