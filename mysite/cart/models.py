from django.db import models
from shop.models import Item

class CartItem(models.Model):
    cart_id = models.CharField(max_length=50)
    date_added = models.DateTimeField(auto_now_add=True)
    quantity = models.IntegerField(default=1)
    item = models.ForeignKey('shop.Item', unique=False)

    class Meta:
        db_table = 'cart_items'
        ordering = ['date_added']

    def total(self):
        return self.quantity * self.item.price

    def name(self):
        return self.item.name

    def price(self):
        return self.item.price

    def get_absolute_url(self):
        return self.item.get_absolute_url()

    def augment_quantity(self, quantity):
        self.quantity = self.quantity + int(quantity)
        self.save()


class Order_Status(models.Model):
    CONFIRMATION = ''
    WAITING_FOR_DELIEVERY = ''
    SOLD_WAITING_FOR_DELIEVERY = ''
    SOLD = ''
        
    status_choices = (
        (CONFIRMATION, 'Ждёт подтверждения'),
        (WAITING_FOR_DELIEVERY, 'Ждет доставки'),
        (SOLD_WAITING_FOR_DELIEVERY, 'Продано, ждет доставки'),
        (SOLD, 'Продано'),
    )

    status_name = models.CharField(max_length=50, choices=status_choices, default=CONFIRMATION)

    def __str__(self):
        return self.status_name

class Order(models.Model):
    WHEN_DELIEVERED = ''
    CARD = ''

    payment_choices = (
        (WHEN_DELIEVERED, 'Наличными курьеру'),
        (CARD, 'Оплата картой'),
    )

    order_id = models.CharField(max_length=50)
    item = models.ManyToManyField(CartItem)
    phone = models.CharField(max_length=50)
    adress = models.CharField(max_length=200, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = models.ForeignKey(Order_Status)
    subtotal = models.DecimalField(max_digits=15, decimal_places=2)
    payment = models.CharField(max_length=50, choices=payment_choices, default=WHEN_DELIEVERED)


# Create your models here.
