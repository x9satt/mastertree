from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.core.mail import send_mail, EmailMultiAlternatives
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from . import cart
CART_ID_SESSION_KEY = 'cart_id'

def show_cart(request, template_name="cart/cart.html"):
    if request.method == 'POST':
        postdata = request.POST.copy()
        if postdata['submit'] == 'X':
            cart.remove_from_cart(request)
        if postdata['submit'] == 'Обновить':
            cart.update_cart(request)
    cart_items = cart.get_cart_items(request)
#    cart_item_count = cart.cart_item_count(request)
    page_title = 'Корзина'
    cart_subtotal = cart.cart_subtotal(request)
    return render_to_response(template_name, locals(), context_instance=RequestContext(request))

def checkout(request, template_name="cart/checkout.html"):
    cart_items = cart.get_cart_items(request)
    page_title = 'Оформление заказа'
    cart_subtotal = cart.cart_subtotal(request)
    return render_to_response(template_name, locals(), context_instance=RequestContext(request))




# отправка информации о заказе на почту
def send_email(request, template_name="cart/send.html"):


    if request.method == 'POST':
        postdata = request.POST.copy()
        name = postdata.get('name','')
        email = postdata.get('email','')
        phone = postdata.get('phone','')
        adress = postdata.get('adress','')
        comment = postdata.get('comment','')
        obtaining = postdata.get('obtaining','')
        #paytype = postdata.get('paytype','') /// </p><p> Способ оплаты: '+ paytype +'; - из строки html-content

        cart_items = cart.get_cart_items(request)
        cart_subtotal = cart.cart_subtotal(request)
        # тема и почтовые ящики(от кого и кому)
        subject, from_email, to = 'Новый заказ ', 'sender@mastertree.ru', 'info@mastertree.ru'
        text_content = ''

        itemhtml = ''
        for item in cart_items:
            itemhtml+='Наименование: <strong>' + item.item.item_name + '</strong>; Количество: <strong>' + str(item.quantity) + '</strong>.<br/>'    

        html_content = '<p> ФИО: '+ name +';</p><p> Почта: '+ email +';</p><p>Телефон: '+ str(phone) +';</p><p> Адресс: '+ adress +';</p></p> Примечание: '+ comment +';</p><p> Способ получения: '+ obtaining +';</p> <p>Сумма: <strong>'+ str(cart_subtotal)+ '</strong>.</p></br>' + itemhtml
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()

        subject_4customer, from_email_4customer, to_4customer = 'Ваш заказ принят', 'info@mastertree.ru', email
        text_content_4customer = ''
        msg_to_customer = EmailMultiAlternatives(subject_4customer, text_content_4customer, from_email_4customer, [to_4customer])
        html_content_to_customer = '<p>Телефон: '+ str(phone) +';</p><p> Адресс: '+ adress +';</p><p> Способ получения: '+ obtaining +';</p> <p>Сумма: <strong>'+ str(cart_subtotal)+ '</strong>.</p></br>' + itemhtml
        msg_to_customer.attach_alternative(html_content_to_customer, "text/html")
        msg_to_customer.send()

        page_title = 'Оформление заказа'
        request.session[CART_ID_SESSION_KEY]=''
        return render_to_response(template_name, locals(), context_instance=RequestContext(request))
    else: return HttpResponseRedirect(reverse('home'))


# Create your views here.