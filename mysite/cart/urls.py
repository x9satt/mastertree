from django.conf.urls import *
from . import views
from django.conf.urls import url, patterns
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name = 'cart'
urlpatterns = [
    url(r'^$', views.show_cart, {'template_name':'cart/cart.html' }, name='show_cart'),
    url(r'^checkout/$', views.checkout, name='checkout'),
    url(r'^send/$', views.send_email, name='send_email'),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#urlpatterns += staticfiles_urlpatterns()
