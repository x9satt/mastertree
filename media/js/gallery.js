$('.magnific-all').each(function() {
  var $container = $(this);
  var $imageLinks = $container.find('.item');
 
  var items = [];
  $imageLinks.each(function() {
    var $item = $(this);
    var type = 'image';
    if ($item.hasClass('magnific-youtube')) {
      type = 'iframe';
    }
    var magItem = {
      src: $item.attr('href'),
      type: type
    };
    magItem.title = $item.data('title');
    items.push(magItem);
    });
 
  $imageLinks.magnificPopup({
    mainClass: 'mfp-fade',
    items: items,
    gallery:{
        enabled:true,
        tPrev: $(this).data('prev-text'),
        tNext: $(this).data('next-text')
    },
    type: 'image',
    callbacks: {
      beforeOpen: function() {
        var index = $imageLinks.index(this.st.el);
        if (-1 !== index) {
          this.goTo(index);
        }
      }
    }
  });
});
$('.test-popup-link').magnificPopup({
  type: 'image'
  // other options
});