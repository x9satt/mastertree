ymaps.ready(init);
    var myMap, 
        myPlacemark;

    function init(){ 
        myMap = new ymaps.Map("map", {
            center: [58.521586, 31.223557],
            zoom: 16,
            controls: [],
        });

        myPlacemark = new ymaps.Placemark([58.521586, 31.223557], {
            hintContent: 'Наше производство',
            balloonContent: '<h4 style="padding-bottom:-10px">Master Tree</h4><p>Великий Новгород</p><p>ул.Нехинская 59Б</p><p style="padding-bottom:-15px;">Телефон: +7 (911) 600-78-88</p>',
        });

        myMap.geoObjects.add(myPlacemark);

        myPlacemark.events
            .add('mouseenter', function (e) {
            // Ссылку на объект, вызвавший событие,
            // можно получить из поля 'target'.
                e.get('target').options.set('preset', 'islands#greenIcon');
            })
            .add('mouseleave', function (e) {
                e.get('target').options.unset('preset');
            });

        myMap.controls.add('geolocationControl');
        myMap.controls.add('routeEditor');
        myMap.controls.add('fullscreenControl');
        myMap.controls.add('zoomControl');
    }