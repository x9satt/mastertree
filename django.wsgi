import os
import sys
sys.path.append('/home/host***/mastertree.ru/ENV1/')
sys.path.append('/home/host***/mastertree.ru/htdocs/www')
sys.path.append('/home/host***/mastertree.ru/htdocs/www/mysite')
with open("/home/host***/mastertree.ru/ENV1/bin/activate_this.py") as f:
    code = compile(f.read(), "/home/host***/mastertree.ru/ENV1//bin/activate_this.py", "exec")
    exec(code, dict(__file__="/home/host***/mastertree.ru/ENV1//bin/activate_this.py"))
os.environ['DJANGO_SETTINGS_MODULE'] = 'mysite.settings'

from django.core.wsgi import get_wsgi_application
from whitenoise.django import DjangoWhiteNoise
application = DjangoWhiteNoise(get_wsgi_application())